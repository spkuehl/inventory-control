from django.contrib import admin
from .models import Owner, Checkout, Checkin


class OwnerAdmin(admin.ModelAdmin):
    model = Owner
    list_display = ('pin', 'first_name', 'last_name')


class CheckoutAdmin(admin.ModelAdmin):
    model = Checkout
    list_display = ('user', 'label', 'quantity', 'date_time_out')


class CheckinAdmin(admin.ModelAdmin):
    model = Checkin
    list_display = ('user', 'label', 'quantity', 'date_time_in')


admin.site.register(Owner, OwnerAdmin)
admin.site.register(Checkout, CheckoutAdmin)
admin.site.register(Checkin, CheckinAdmin)

from django.db import models
from label_control.models import Label


class Owner(models.Model):
    first_name = models.CharField(
        max_length=15
        )

    last_name = models.CharField(
        max_length=40
        )

    pin = models.SmallIntegerField()

    @staticmethod
    def autocomplete_search_fields():
        return ("pin__icontains", "name__icontains",)

    def __str__(self):
        return self.last_name


class Checkout(models.Model):
    user = models.ForeignKey(Owner)
    label = models.ForeignKey(Label)
    date_time_out = models.DateTimeField(auto_now_add=True)
    quantity = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.label


class Checkin(models.Model):
    user = models.ForeignKey(Owner)
    label = models.ForeignKey(Label)
    date_time_in = models.DateTimeField(auto_now_add=True)
    quantity = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.label

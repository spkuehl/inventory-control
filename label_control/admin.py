from django.contrib import admin
from .models import Brand, UnitOfMeasure, Label, LabelOrder


class BrandAdmin(admin.ModelAdmin):
    model = Brand
    list_display = ('name',)


class UnitOfMeasureAdmin(admin.ModelAdmin):
    model = UnitOfMeasure
    list_display = ('name',)


class LabelAdmin(admin.ModelAdmin):
    model = Label
    list_display = ('code', 'name', 'brand_name', 'unit_of_measure', 'active')


class LabelOrderAdmin(admin.ModelAdmin):
    model = LabelOrder
    list_display = ('label', 'quantity', 'purchase_order', 'date_ordered')


admin.site.register(Brand, BrandAdmin)
admin.site.register(UnitOfMeasure, UnitOfMeasureAdmin)
admin.site.register(Label, LabelAdmin)
admin.site.register(LabelOrder, LabelOrderAdmin)

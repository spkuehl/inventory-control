from django.apps import AppConfig


class LabelControlConfig(AppConfig):
    name = 'label_control'

from .models import Brand, UnitOfMeasure, Label, LabelOrder
from rest_framework import serializers


class GameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class UnitOfMeasureSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UnitOfMeasure
        fields = '__all__'


class LabelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Label
        fields = '__all__'


class LabelOrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LabelOrder
        fields = '__all__'

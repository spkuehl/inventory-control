from .models import (
    Brand, UnitOfMeasure, Label, LabelOrder
    )
from rest_framework import viewsets
from .serializers import (
    BrandSerializer,
    UnitOfMeasureSerializer,
    LabelSerializer,
    LabelOrderSerializer,
    )


class BrandViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Brand to be viewed or edited.
    """
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class UnitOfMeasureViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows U/M to be viewed or edited.
    """
    queryset = UnitOfMeasure.objects.all()
    serializer_class = UnitOfMeasureSerializer


class LabelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Label to be viewed or edited.
    """
    queryset = Label.objects.all()
    serializer_class = LabelSerializer


class LabelOrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Label Order to be viewed or edited.
    """
    queryset = LabelOrder.objects.all()
    serializer_class = LabelOrderSerializer

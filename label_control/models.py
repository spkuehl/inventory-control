from django.db import models
from django.utils.timezone import now


class Brand(models.Model):
    name = models.CharField(
        max_length=100
        )

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)

    def __str__(self):
        return self.name


class UnitOfMeasure(models.Model):
    class Meta:
        ordering = ['name', ]

    name = models.CharField(
        max_length=100
        )

    def __str__(self):
        return self.name


class Label(models.Model):
    code = models.IntegerField(
        unique=True
        )

    name = models.CharField(
        max_length=144,
        )

    brand_name = models.ForeignKey(
        Brand,
        help_text="Type the brand name, if it does not appear click the icon to browse the list or create a new one."  # noqa
        )

    unit_of_measure = models.ForeignKey(
        UnitOfMeasure
        )

    description = models.CharField(
        max_length=144, help_text="Enter a short description of the product.",
        null=True, blank=True
        )

    active = models.BooleanField(
        default=True, help_text="Uncheck if product is retired."
        )

    date_created = models.DateField(
        default=now
        )

    used_roll = models.BooleanField(
        default=False
        )

    @staticmethod
    def autocomplete_search_fields():
        return ("code__icontains",)

    def __str__(self):
        return str(self.code)


class LabelOrder(models.Model):
    label = models.ForeignKey(
        Label
        )

    quantity = models.IntegerField()

    purchase_order = models.CharField(max_length=15)

    date_ordered = models.DateField(default=now)
